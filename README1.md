/v1.4 details added.
////Details added TES-19-v1.2release
//Details added TES-17-hotfix @ 13.15 pm
//Details added @ branch level @13.15 pm
//Details added@ branch level 13.00PM
//Details added to brnach TES-16-codefix5
//Details added @ branch level. @12.53 PM
//TES-15-codefix4 details added to branch
// Details added @ branch level.
//Details added git checkout TES-14-hotfix3.
//Details added @ branch level 
//TES-13-codefix1 Details added @ branch.
//18-Apr-2022 @10.25 AM Hotfix details added.
//18-Apr-2022 @10.17 AM Data Fix details added.
//17-Apr-2022 @9.24 hot fix details added.
//16-Apr-2022 @15.01 new comment line added to this file.
//16-Apr-2022 @14.43  new comment line added to this file.
//16-Apr-2022 @15.09 new comment line added to this file.

## Project Description/Summary

This project involves wiring up a simple calculator.

## New Things Learned or Refreshed

I spent most my time starring at the screen.I got my numbers to show up but I couldn't figure out how to do the math and make it show up.I thought the easiest way was to use the eval() function but I read in the MDN documentation to never use eval(). So, I got stuck and lost patience.I remember doing a JavaScript calculator project about 5 months ago but it was clear today that that I must have just copied the solution as a code along as Brian Holt typed through it. The code for my previous JavaScript Calculator project is NOTHING like the above code.I ended up watching John's solution and implemented his since I had already been at this problem for more than an hour. Word of caution, the solution above works to the extent that the user uses the calculator flawlessly. If not, there are many use cases in which the code won't work and will throw errors. For example, if someone starts with an * operator instead of a number, the eval() function won't work. You'll get an Uncaught SyntaxError: Unexpected token * at HTMLButtonElement.<anonymous> error.

## Time to Code

This took about 2 hours, start to finish, including watching John's solution and then going back to
 finish my own solution.Lines of CodeThis took 30 lines of code, including comments and white space.

## Biggest Take Away(s)

I couldn't for the life of me remember how I coded my last calculator project.
 This goes to show that continuous coding and going back to previous problems is just the name of the game. 

//This is comments section

//Branch1 updates
//This is addition of new line  