//v1.4 details added.
//V1.3 details added. 
//Details added TES-19-v1.2release
//Details added to branch @ branch level 
//Details added to TES-17-hotfix @ 13.14PM
//Details added @ Master @13.06PM
//Details added @ brnach TES-16-codefix5 @13.01PM
//Details added @ branch levelTES-15-codefix4
//added changes in master 
//18-Apr-2022@10.53 AM content added thorugh other brach
//18-Apr-2022@10.25 AM Hot Fix details added. 
//18-Apr-2022@10.17 AM Data Fix added
//17-Apr-2022@9.25 hot fix code added. 
//17-Apr-2022@9.23 hot fix details added.
//17-Apr-2022@09.10 feature5 addition
//16-Apr-2022@20.53 feature5 added.
//16-Apr-2022@20.42 feature4 added.
//16-Apr-2022@20.34 feature3 added.
//16-Apr-2022 @20.10 Feature2 deatils added. 
//16-Apr-2022@20.00 New Feature details added.
//16-Apr-2022 @7.15 PM  Hot fix comments added.
// 16-Apr-2022  @7PM   new comments added.

**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

//Comments added 
/Additional comments for  second bracnh

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).